const mongoose = require('mongoose')
const PatientSchema = new mongoose.Schema({
    name: String,
    PhoneNo: Number,
    Age: Number,
    Gender : String,
    FatherName: String,
    Address: String,
    UserName: String })
module.exports = mongoose.model('patient', PatientSchema)