const express = require('express')
const router = express.Router()
const { getDatas,getData,CreateData,UpdateData,DeleteData } = require('../controller/Patient')
router.route('/').get(getDatas).post(CreateData)
router.route('/:id').get(getData).put(UpdateData).delete(DeleteData)
module.exports = router