const ErrorResponse = require('../utils/errorResponse')
const patient = require('../models/patient')

exports.getDatas = async (req, res, next) => {

    const getit = await patient.find()
    res.json({
        success: true, data: getit
    })
}

exports.getData = async (req, res, next) => {
    try{
    const getit = await patient.findById(req.params.id)
    if(!getit){
        return next(
            new ErrorResponse(`Data not found with id of${req.params.id}` ,404
        ))  }

    res.json({success: true, data: getit})
} catch (err)
{
    next(new ErrorResponse(`Data not found with id of${req.params.id}` ,404))
} }
exports.CreateData = async (req, res, next) => {
    console.log(req.body)
    const mydata = await patient.create(req.body)
    res.json({success: true, data: mydata})
    
}

exports.UpdateData = async (req, res, next) => {
    const mydata = await patient.findByIdAndUpdate(req.params.id, req.body)
    res.json({success: true, data: mydata})
}

exports.DeleteData = async (req, res, next) => {
    const mydata = await patient.findByIdAndDelete(req.params.id)
    res.json({success: true, data: {}})
}